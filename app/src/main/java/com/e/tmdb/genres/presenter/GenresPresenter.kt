package com.e.tmdb.genres.presenter

import android.content.Context
import com.e.tmdb.api.TMDBApi
import com.e.tmdb.genres.model.Genres
import com.e.tmdb.genres.view.GenresView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenresPresenter(val context: Context, val view: GenresView) {
    fun getGenres(apiKey: String) {
        view.showLoading()
        val genres = TMDBApi.create()
        genres.getGenres(apiKey).enqueue(object : Callback<Genres> {
            override fun onFailure(call: Call<Genres>, t: Throwable) {
                view.showMessage(t.message!!)
                view.hideLoading()
            }

            override fun onResponse(call: Call<Genres>, response: Response<Genres>) {
                view.showGenres(response.body()?.genres!!)
                view.hideLoading()
            }
        })
    }
}