package com.e.tmdb.genres.view

import com.e.tmdb.genres.model.Genre

interface GenresView {
    fun showGenres(genres: List<Genre>)
    fun showMessage(message: String)
    fun showLoading()
    fun hideLoading()
}