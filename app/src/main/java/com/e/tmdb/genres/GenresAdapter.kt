package com.e.tmdb.genres

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.e.tmdb.R
import com.e.tmdb.genres.model.Genre
import com.e.tmdb.movies.MoviesActivity
import kotlinx.android.synthetic.main.item_genres.view.*

class GenresAdapter(private val genres: ArrayList<Genre>, val context: Context) :
    RecyclerView.Adapter<GenresAdapter.GenresViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): GenresViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_genres, p0,
            false)

        return GenresViewHolder(view)
    }

    override fun getItemCount(): Int = genres.size


    override fun onBindViewHolder(p0: GenresViewHolder, p1: Int) {
        p0.bindItem(genres[p1])
    }

    class GenresViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(genre: Genre) {
            val context = itemView.context
            itemView.textGenre.setText(genre.name)

            itemView.setOnClickListener {
                val intent = Intent(context, MoviesActivity::class.java)
                intent.putExtra("id", genre.id)
                context.startActivity(intent)
            }
        }
    }
}