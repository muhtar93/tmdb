package com.e.tmdb.genres

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.e.tmdb.R
import com.e.tmdb.genres.model.Genre
import com.e.tmdb.genres.presenter.GenresPresenter
import com.e.tmdb.genres.view.GenresView
import kotlinx.android.synthetic.main.activity_genres.*


class GenresActivity : AppCompatActivity(), GenresView {
    private lateinit var presenter: GenresPresenter
    private lateinit var adapter: GenresAdapter
    private var list: ArrayList<Genre> = java.util.ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genres)

        presenter = GenresPresenter(this, this)
        presenter.getGenres("9c6bece01022caf8c49133cd1f7a6b1e")

        adapter = GenresAdapter(list, this)

        listGenres.layoutManager = LinearLayoutManager(this)
        listGenres.adapter = adapter
        listGenres.addItemDecoration(
            DividerItemDecoration(
                listGenres.getContext(),
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun showGenres(genres: List<Genre>) {
        list.addAll(genres)
        adapter.notifyDataSetChanged()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading.visibility = View.INVISIBLE
    }
}