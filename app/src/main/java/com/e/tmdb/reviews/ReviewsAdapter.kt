package com.e.tmdb.reviews

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.e.tmdb.R
import com.e.tmdb.movies.MoviesActivity
import com.e.tmdb.reviews.model.Review
import kotlinx.android.synthetic.main.item_reviews.view.*

class ReviewsAdapter(private val reviews: ArrayList<Review>, val context: Context) :
    RecyclerView.Adapter<ReviewsAdapter.GenresViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): GenresViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_reviews, p0,
            false)

        return GenresViewHolder(view)
    }

    override fun getItemCount(): Int = reviews.size


    override fun onBindViewHolder(p0: GenresViewHolder, p1: Int) {
        p0.bindItem(reviews[p1])
    }

    class GenresViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(review: Review) {
            val context = itemView.context
            itemView.textAuthor.setText(review.author)
            itemView.textContent.setText(review.content)
        }
    }
}