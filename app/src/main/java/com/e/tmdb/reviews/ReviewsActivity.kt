package com.e.tmdb.reviews

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.e.tmdb.R
import com.e.tmdb.consts.Networks
import com.e.tmdb.reviews.model.Review
import com.e.tmdb.reviews.presenter.ReviewsPresenter
import com.e.tmdb.reviews.view.ReviewsView
import kotlinx.android.synthetic.main.activity_reviews.*

class ReviewsActivity : AppCompatActivity(), ReviewsView {
    private lateinit var presenter: ReviewsPresenter
    private lateinit var adapter: ReviewsAdapter
    private var list: ArrayList<Review> = java.util.ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reviews)

        presenter = ReviewsPresenter(this, this)
        Log.d("id", intent.getIntExtra("id", 0).toString())
        presenter.getReviews(intent.getIntExtra("id", 0), Networks.API_KEY)

        adapter = ReviewsAdapter(list, this)

        listReviews.layoutManager = LinearLayoutManager(this)
        listReviews.adapter = adapter
        listReviews.addItemDecoration(
            DividerItemDecoration(
                listReviews.getContext(),
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun showReviews(reviews: List<Review>) {
        Log.d("showReviews", reviews.toString())
        if (reviews.size !== 0) {
            list.addAll(reviews)
            adapter.notifyDataSetChanged()
        } else {
            textEmpty.setText("We don't have any reviews about this movie")
            textEmpty.visibility = View.VISIBLE
        }
    }

    override fun showMessage(message: String) {
    }

    override fun showLoading() {
    }

    override fun hideLoading() {
    }
}