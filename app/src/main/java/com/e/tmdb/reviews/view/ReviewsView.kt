package com.e.tmdb.reviews.view

import com.e.tmdb.reviews.model.Review

interface ReviewsView {
    fun showReviews(reviews: List<Review>)
    fun showMessage(message: String)
    fun showLoading()
    fun hideLoading()
}