package com.e.tmdb.reviews.model

import com.google.gson.annotations.SerializedName

data class Review(
    @SerializedName("author")
    val author: String = "",
    @SerializedName("content")
    val content: String = ""
)