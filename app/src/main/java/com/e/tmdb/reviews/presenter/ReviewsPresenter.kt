package com.e.tmdb.reviews.presenter

import android.content.Context
import com.e.tmdb.api.TMDBApi
import com.e.tmdb.reviews.model.Reviews
import com.e.tmdb.reviews.view.ReviewsView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReviewsPresenter (val context: Context, val view: ReviewsView) {
    fun getReviews(movieId: Int, apiKey: String) {
        view.showLoading()
        val reviews = TMDBApi.create()
        reviews.getReviews(movieId, apiKey).enqueue(object : Callback<Reviews> {
            override fun onFailure(call: Call<Reviews>, t: Throwable) {
                view.showMessage(t.message!!)
                view.hideLoading()
            }
            override fun onResponse(call: Call<Reviews>, response: Response<Reviews>) {
                view.showReviews(response.body()?.results!!)
                view.hideLoading()
            }
        })
    }
}