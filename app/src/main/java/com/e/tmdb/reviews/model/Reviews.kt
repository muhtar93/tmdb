package com.e.tmdb.reviews.model

import com.google.gson.annotations.SerializedName

data class Reviews(
    @SerializedName("results")
    val results: List<Review>
)