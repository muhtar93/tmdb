package com.e.tmdb.movies.view

import com.e.tmdb.movies.model.Movie

interface MoviesView {
    fun showMovies(movies: List<Movie>)
    fun showMessage(message: String)
    fun showLoading()
    fun hideLoading()
}