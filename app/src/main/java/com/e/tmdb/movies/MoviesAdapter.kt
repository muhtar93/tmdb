package com.e.tmdb.movies

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.e.tmdb.R
import com.e.tmdb.consts.Networks
import com.e.tmdb.detail.DetailActivity
import com.e.tmdb.movies.model.Movie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movies.view.*

class MoviesAdapter(private val movies: ArrayList<Movie>, val context: Context) :
    RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MoviesViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.item_movies, p0,
            false)

        return MoviesViewHolder(view)
    }

    override fun getItemCount(): Int = movies.size


    override fun onBindViewHolder(p0: MoviesViewHolder, p1: Int) {
        p0.bindItem(movies[p1])
    }

    class MoviesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(movie: Movie) {
            val context = itemView.context

            Picasso.get().load(Networks.CDN + movie.posterPath)
                .into(itemView.imageMovie)

            itemView.setOnClickListener {
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("id", movie.id)
                context.startActivity(intent)
            }
        }
    }
}