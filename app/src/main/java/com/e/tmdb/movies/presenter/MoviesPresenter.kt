package com.e.tmdb.movies.presenter

import android.content.Context
import com.e.tmdb.api.TMDBApi
import com.e.tmdb.movies.model.Movies
import com.e.tmdb.movies.view.MoviesView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviesPresenter(val context: Context, val view: MoviesView) {
    fun getMovies(id: Int, page: Int, apiKey: String) {
        view.showLoading()
        val movies = TMDBApi.create()
        movies.getMovies(id, page, apiKey).enqueue(object : Callback<Movies>{
            override fun onFailure(call: Call<Movies>, t: Throwable) {
                view.showMessage(t.message!!)
                view.hideLoading()
            }

            override fun onResponse(call: Call<Movies>, response: Response<Movies>) {
                view.showMovies(response.body()?.results!!)
                view.hideLoading()
            }
        })
    }
}