package com.e.tmdb.movies.model

import com.google.gson.annotations.SerializedName

data class Movies(
    @SerializedName("results")
    val results: List<Movie>
)