package com.e.tmdb.movies

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.e.tmdb.R
import com.e.tmdb.consts.Networks
import com.e.tmdb.movies.model.Movie
import com.e.tmdb.movies.presenter.MoviesPresenter
import com.e.tmdb.movies.view.MoviesView
import kotlinx.android.synthetic.main.activity_movies.*

class MoviesActivity : AppCompatActivity(), MoviesView {
    private lateinit var presenter: MoviesPresenter
    private lateinit var adapter: MoviesAdapter
    private var list: ArrayList<Movie> = java.util.ArrayList()
    private var page: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)

        val id = intent.getIntExtra("id", 0)

        presenter = MoviesPresenter(this, this)
        presenter.getMovies(id, page, Networks.API_KEY)

        adapter = MoviesAdapter(list, this)

        listMovies.layoutManager = LinearLayoutManager(this)
        listMovies.adapter = adapter
        listMovies.addItemDecoration(
            DividerItemDecoration(
                listMovies.getContext(),
                DividerItemDecoration.VERTICAL
            )
        )

        initListener()
    }

    private fun initListener() {
        val id = intent.getIntExtra("id", 0)
        listMovies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView?.layoutManager as LinearLayoutManager
                val countItem = linearLayoutManager?.itemCount
                val lastVisiblePosition = linearLayoutManager?.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                if (isLastPosition) {
                    page = page + 1
                    presenter.getMovies(id, page, Networks.API_KEY)
                }
            }
        })
    }

    override fun showMovies(movies: List<Movie>) {
        list.addAll(movies)
        adapter.notifyDataSetChanged()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message.toString(), Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading.visibility = View.INVISIBLE
    }
}