package com.e.tmdb.detail.view

import com.e.tmdb.detail.model.Detail

interface DetailView {
    fun showDetail(detail: Detail)
    fun showMessage(message: String)
    fun getKey(key: String)
}