package com.e.tmdb.detail.model

import com.e.tmdb.genres.model.Genre
import com.google.gson.annotations.SerializedName

data class Detail(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("backdrop_path")
    val backdropPath: String = "",
    @SerializedName("title")
    val originalTitle: String = "",
    @SerializedName("overview")
    val overview: String = "",
    @SerializedName("tagline")
    val tagline: String = "",
    @SerializedName("release_date")
    val releaseDate: String = "",
    @SerializedName("vote_average")
    val voteAverage: String = "",
    @SerializedName("genres")
    val genres: List<Genre>
)