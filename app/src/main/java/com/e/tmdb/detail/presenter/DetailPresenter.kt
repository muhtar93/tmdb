package com.e.tmdb.detail.presenter

import android.content.Context
import com.e.tmdb.api.TMDBApi
import com.e.tmdb.detail.model.Detail
import com.e.tmdb.detail.model.Videos
import com.e.tmdb.detail.view.DetailView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPresenter (val context: Context, val view: DetailView) {
    fun getDetail(apiKey: String, movieId: Int) {
        val detail = TMDBApi.create()
        detail.getDetail(movieId, apiKey).enqueue(object : Callback<Detail> {
            override fun onFailure(call: Call<Detail>, t: Throwable) {
                view.showMessage(t.message!!)
            }
            override fun onResponse(call: Call<Detail>, response: Response<Detail>) {
                view.showDetail(response.body()!!)
            }
        })
    }

    fun getVideo(apiKey: String, movieId: Int) {
        val detail = TMDBApi.create()
        detail.getVideos(movieId, apiKey).enqueue(object : Callback<Videos> {
            override fun onFailure(call: Call<Videos>, t: Throwable) {
                view.showMessage(t.message!!)
            }
            override fun onResponse(call: Call<Videos>, response: Response<Videos>) {
                if (response.body()?.results?.size !== 0) {
                    view.getKey(response.body()?.results?.get(0)!!.key)
                }
            }
        })
    }
}
