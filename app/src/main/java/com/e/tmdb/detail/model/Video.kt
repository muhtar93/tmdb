package com.e.tmdb.detail.model

import com.google.gson.annotations.SerializedName

data class Video(
    @SerializedName("key")
    val key: String = ""
)