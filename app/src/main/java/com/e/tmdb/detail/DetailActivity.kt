package com.e.tmdb.detail

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.e.tmdb.R
import com.e.tmdb.consts.Networks
import com.e.tmdb.detail.model.Detail
import com.e.tmdb.detail.presenter.DetailPresenter
import com.e.tmdb.detail.view.DetailView
import com.e.tmdb.reviews.ReviewsActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), DetailView {
    private lateinit var presenter: DetailPresenter
    private var keyVideo: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val id = intent.getIntExtra("id", 0)

        presenter = DetailPresenter(this, this)
        presenter.getDetail(Networks.API_KEY, id)
        presenter.getVideo(Networks.API_KEY, id)
    }

    override fun showDetail(detail: Detail) {
        detailTitle.setText(detail.originalTitle + " (${detail.releaseDate.substring(0,4)})")
        detailRate.setText("${detail.voteAverage}/10")
        if (detail.genres.size == 6) {
            detailGenre.setText(
                "${detail.genres.get(0).name}, ${detail.genres.get(1).name}, " +
                    "${detail.genres.get(2).name}, ${detail.genres.get(3).name}, " +
                        "${detail.genres.get(4).name}, ${detail.genres.get(5).name}"
            )
        } else if (detail.genres.size == 5) {
            detailGenre.setText(
                "${detail.genres.get(0).name}, ${detail.genres.get(1).name}, " +
                        "${detail.genres.get(2).name}, ${detail.genres.get(3).name}, " +
                        "${detail.genres.get(4).name}"
            )
        } else if (detail.genres.size == 4) {
            detailGenre.setText(
                "${detail.genres.get(0).name}, ${detail.genres.get(1).name}, " +
                        "${detail.genres.get(2).name}, ${detail.genres.get(3).name}"
            )
        } else if (detail.genres.size == 3) {
            detailGenre.setText(
                "${detail.genres.get(0).name}, ${detail.genres.get(1).name}, " +
                        "${detail.genres.get(2).name}"
            )
        } else if (detail.genres.size == 2) {
            detailGenre.setText(
                "${detail.genres.get(0).name}, ${detail.genres.get(1).name}"
            )
        } else {
            detailGenre.setText(
                "${detail.genres.get(0).name}"
            )
        }

        Picasso.get().load(Networks.CDN + detail.backdropPath)
            .into(imageDetail)

        detailOverview.setText(detail.overview)
        if (detail.tagline != "") {
            detailTagline.setText(detail.tagline)
        } else {
            detailTagline.visibility = View.GONE
        }

        btnReview.setOnClickListener {
            val intent = Intent(this, ReviewsActivity::class.java)
            intent.putExtra("id", detail.id)
            startActivity(intent)
        }

        btnWatch.setOnClickListener {
            val intentApp = Intent(Intent.ACTION_VIEW,
                Uri.parse("vnd.youtube:" + keyVideo))
            val intentBrowser = Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + keyVideo))
            try {
                this.startActivity(intentApp)
            } catch (ex: ActivityNotFoundException) {
                this.startActivity(intentBrowser)
            }
        }
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun getKey(key: String) {
        keyVideo = key
    }
}