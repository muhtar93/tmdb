package com.e.tmdb.consts

class Networks {
    companion object {
        const val API_KEY = "9c6bece01022caf8c49133cd1f7a6b1e"
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val CDN = "https://image.tmdb.org/t/p/w500/"
    }
}