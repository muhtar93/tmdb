package com.e.tmdb.api

import com.e.tmdb.R
import com.e.tmdb.consts.Networks
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object TMDBApi {
    var gson = GsonBuilder()
        .setLenient()
        .create()

    fun create(): ApiServices {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(Networks.BASE_URL)
            .build()
        return retrofit.create(ApiServices::class.java)
    }
}