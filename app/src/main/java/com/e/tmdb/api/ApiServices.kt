package com.e.tmdb.api

import com.e.tmdb.detail.model.Detail
import com.e.tmdb.detail.model.Videos
import com.e.tmdb.genres.model.Genres
import com.e.tmdb.movies.model.Movies
import com.e.tmdb.reviews.model.Reviews
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiServices {
    @GET("genre/movie/list")
    fun getGenres(@Query("api_key") apiKey: String): Call<Genres>

    @GET("discover/movie")
    fun getMovies(
        @Query("with_genres") withGenres : Int,
        @Query("page") page: Int,
        @Query("api_key") apiKey: String): Call<Movies>

    @GET("movie/{movie_id}")
    fun getDetail(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey : String): Call<Detail>

    @GET("movie/{movie_id}/reviews")
    fun getReviews(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey : String): Call<Reviews>

    @GET("movie/{movie_id}/videos")
    fun getVideos(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey : String): Call<Videos>
}